#!/bin/bash

kubectl --kubeconfig=kubectl_config.yml version
kubectl --kubeconfig=kubectl_config.yml patch deployment hosco-demo --type=strategic -p \
    '{ "spec": { "template": { "spec": { "containers": [{ "name": "hosco-demo", "image": "'${CONTAINER_IMAGE}'", "env": [{ "name":"CI_JOB_ID", "value":"'${CI_JOB_ID}'" }] }] } } } }'
